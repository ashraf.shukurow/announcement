package com.ingressapp.announcement.repository;

import com.ingressapp.announcement.model.dto.response.AnnouncementResponse;
import com.ingressapp.announcement.model.entity.Announcement;
import com.ingressapp.announcement.model.entity.User;
import com.ingressapp.announcement.spec.AnnouncementSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface AnnouncementRepository extends JpaRepository<Announcement,Long> , JpaSpecificationExecutor<Announcement> {
    @Query("select a from Announcement a order by a.viewCount desc ")
    Page<Announcement> getMostViewedAnnouncements(Pageable pageable);

    @Query("select a from Announcement a where a.user=:user")
    Page<Announcement> findByUser(User user, Pageable pageable);

    @Query("select a from Announcement a where a.user.id=:userId and a.id=:announcementId")
    Optional<Announcement> getOwnAnnouncementWithId(Long announcementId,Long userId);

    @Query("SELECT a FROM Announcement a WHERE a.user.id = :userId AND a.viewCount = (SELECT MAX(a2.viewCount) FROM Announcement a2)")
//    @Query("select a from Announcement a where a.user.id=:userId order by a.viewCount desc limit 1")
    Optional<Announcement> getOwnMostViewedAnnouncement(Long userId);
//    @EntityGraph(attributePaths = {"user"})
//    Page<Announcement> findAll(AnnouncementSpecification announcementSpecification,Pageable pageable);
}
