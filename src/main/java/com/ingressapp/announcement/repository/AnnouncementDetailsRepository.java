package com.ingressapp.announcement.repository;

import com.ingressapp.announcement.model.entity.AnnouncementDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnnouncementDetailsRepository extends JpaRepository<AnnouncementDetails,Long> {
}
