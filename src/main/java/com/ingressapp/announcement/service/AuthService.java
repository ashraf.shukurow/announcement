package com.ingressapp.announcement.service;

import com.ingressapp.announcement.model.dto.request.LoginRequest;
import com.ingressapp.announcement.model.dto.request.UserRequest;
import com.ingressapp.announcement.model.dto.response.TokenResponse;
import com.ingressapp.announcement.model.dto.response.UserResponse;

public interface AuthService {
    TokenResponse login(LoginRequest loginRequest);
    UserResponse register(UserRequest userRequest);
}
