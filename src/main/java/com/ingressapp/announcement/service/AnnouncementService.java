package com.ingressapp.announcement.service;

import com.ingressapp.announcement.model.dto.request.AnnouncementRequest;
import com.ingressapp.announcement.model.dto.request.PageDto;
import com.ingressapp.announcement.model.dto.request.SearchCriteria;
import com.ingressapp.announcement.model.dto.response.AnnouncementResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AnnouncementService {

    Page<AnnouncementResponse> getAllAnnouncement(List<SearchCriteria> searchCriteriaList, Pageable pageable);
    Page<AnnouncementResponse> getMostViewedAnnouncements(PageDto pageRequest);
    void createAnnouncement(AnnouncementRequest announcementRequest);
    void updateAnnouncement(Long id,AnnouncementRequest announcementRequest);
    void deleteAnnouncement(Long id);
    Page<AnnouncementResponse> getAllOwnAnnouncement(Long userId, Pageable pageable);
    AnnouncementResponse getOwnAnnouncementWithId(Long id,Long userId);
    AnnouncementResponse getOwnMostViewedAnnouncement(Long userId);
}
