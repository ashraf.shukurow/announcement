package com.ingressapp.announcement.service.impl;

import com.ingressapp.announcement.exceptions.NotFoundException;
import com.ingressapp.announcement.model.entity.Authority;
import com.ingressapp.announcement.model.entity.User;
import com.ingressapp.announcement.repository.UserRepository;
import com.ingressapp.announcement.security.UserPrincipal.UserPrincipal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = userRepository.findByUsername(username);
        Set<GrantedAuthority> authorities = new HashSet<>();
        if (userOptional.isPresent()) {

            User user = userOptional.get();
            for (Authority role : user.getAuthorities()) {
                authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getAuthority()));
            }
            UserPrincipal userPrincipal = new UserPrincipal();
            userPrincipal.setUsername(user.getUsername());
            userPrincipal.setPassword(user.getPassword());
//            authorities.add(new SimpleGrantedAuthority("ROLE_" + user));
            userPrincipal.setAuthorities(authorities);
            return userPrincipal;
        } else {
            throw new NotFoundException(username + " not found");
        }
    }
}
