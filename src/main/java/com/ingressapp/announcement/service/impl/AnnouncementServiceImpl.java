package com.ingressapp.announcement.service.impl;

import com.ingressapp.announcement.exceptions.AlreadyExistsException;
import com.ingressapp.announcement.exceptions.NotAuthenticatedException;
import com.ingressapp.announcement.exceptions.NotFoundException;
import com.ingressapp.announcement.mapper.AnnouncementMapper;
import com.ingressapp.announcement.model.dto.request.AnnouncementRequest;
import com.ingressapp.announcement.model.dto.request.PageDto;
import com.ingressapp.announcement.model.dto.request.SearchCriteria;
import com.ingressapp.announcement.model.dto.response.AnnouncementResponse;
import com.ingressapp.announcement.model.entity.Announcement;
import com.ingressapp.announcement.model.entity.User;
import com.ingressapp.announcement.repository.AnnouncementRepository;
import com.ingressapp.announcement.repository.UserRepository;
import com.ingressapp.announcement.service.AnnouncementService;
import com.ingressapp.announcement.service.AuthService;
import com.ingressapp.announcement.spec.AnnouncementSpecification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AnnouncementServiceImpl implements AnnouncementService {
    private final AnnouncementRepository announcementRepository;
    private final AnnouncementMapper announcementMapper;
    private final UserRepository userRepository;
    private final AuthService authService;

    @Override
    public Page<AnnouncementResponse> getAllAnnouncement(List<SearchCriteria> searchCriteriaList, Pageable pageable) {
        AnnouncementSpecification announcementSpecification = new AnnouncementSpecification();
        searchCriteriaList.forEach(announcementSpecification::add);
        return announcementRepository
                .findAll(announcementSpecification, pageable)
                .map(announcementMapper::entityToResponse);
    }

    @Override
    public Page<AnnouncementResponse> getMostViewedAnnouncements(PageDto pageDto) {
        return announcementRepository.
                getMostViewedAnnouncements(PageRequest.of(pageDto.getPageNumber(), pageDto.getPageSize(), Sort.by(pageDto.getSort()[0]).descending()))
                .map(announcementMapper::entityToResponse);
    }

    @Override
    public void createAnnouncement(AnnouncementRequest announcementRequest) {
        log.info("trying to check user by userId {}", announcementRequest.getUserId());
        Announcement announcement = announcementMapper.requestToEntity(announcementRequest);
        log.info("starting to check announcement {} ", announcement.getId());
        if (announcementRepository.existsById(announcement.getId())) {
            throw new AlreadyExistsException("Announcement already exits with id: " + announcement.getId());
        }
        announcementRepository.save(announcement);
    }

    @Override
    public void updateAnnouncement(Long id, AnnouncementRequest announcementRequest) {
        Announcement announcement = announcementRepository.findById(id).orElseThrow(() -> new NotFoundException("Announcement not fount with id: " + id));
        Announcement announcement1 = announcementMapper.requestToEntity(announcementRequest);
        announcement1.setId(announcement.getId());
        announcementRepository.save(announcement1);

    }

    @Override
    public void deleteAnnouncement(Long id) {
        Announcement announcement = announcementRepository.findById(id).orElseThrow(() -> new NotFoundException("Announcement not fount with id: " + id));
        announcementRepository.delete(announcement);
    }

    @Override
    @Cacheable(cacheNames = "announcements", key = "#userId + '-' + #pageable.pageNumber + '-' + #pageable.pageSize")
    public Page<AnnouncementResponse> getAllOwnAnnouncement(Long userId, Pageable pageable) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("User not found with id: " + userId));
        Page<Announcement> announcement = announcementRepository.findByUser(user, pageable);

        return announcement.map(announcementMapper::entityToResponse);

    }

    @Override
    public AnnouncementResponse getOwnAnnouncementWithId(Long announcementId, Long userId) {

        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("User Not Found with id: " + userId));
        Announcement announcement = announcementRepository.getOwnAnnouncementWithId(announcementId, userId).orElseThrow(() -> new NotFoundException("Announcement nod found with id: " + announcementId));
        return announcementMapper.entityToResponse(announcement);
    }

    @Override
    public AnnouncementResponse getOwnMostViewedAnnouncement(Long userId) {
        Announcement announcement = announcementRepository.getOwnMostViewedAnnouncement(userId).orElseThrow(() -> new NotFoundException("User Not found with id: " + userId));
        return announcementMapper.entityToResponse(announcement);
    }
}
