package com.ingressapp.announcement.service.impl;

import com.ingressapp.announcement.exceptions.AlreadyExistsException;
import com.ingressapp.announcement.exceptions.NotFoundException;
import com.ingressapp.announcement.mapper.UserMapper;
import com.ingressapp.announcement.model.dto.request.LoginRequest;
import com.ingressapp.announcement.model.dto.request.UserRequest;
import com.ingressapp.announcement.model.dto.response.TokenResponse;
import com.ingressapp.announcement.model.dto.response.UserResponse;
import com.ingressapp.announcement.model.entity.Authority;
import com.ingressapp.announcement.model.entity.User;
import com.ingressapp.announcement.repository.UserRepository;
import com.ingressapp.announcement.security.JwtTokenProvider;
import com.ingressapp.announcement.security.PasswordEncoder;
import com.ingressapp.announcement.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    @Override
    public TokenResponse login(LoginRequest loginRequest) {
        User user=userRepository.findByUsername(loginRequest.getUsername())
                .orElseThrow(()->new NotFoundException("User not found with username: "+loginRequest.getUsername()));
        Authentication authentication=authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(),
                loginRequest.getPassword()
        ));
        TokenResponse tokenResponse=new TokenResponse();
        tokenResponse.setAccessToken(jwtTokenProvider.generateToken((UserDetails) authentication.getPrincipal()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return tokenResponse;

    }


    @Override
    public UserResponse register(UserRequest userRequest) {
        User user=userMapper.requestToEntity(userRequest);
        if(userRepository.existsByUsername(user.getUsername())){
            throw new AlreadyExistsException("Registration is already exists with this username:" + userRequest.getUsername());
        }
        user.setPassword(passwordEncoder.passwordEncode(userRequest.getPassword()));
        Authority authority=new Authority();
        authority.setAuthority("USER");
        user.setAuthorities(Set.of(authority));
        userRepository.save(user);
        return userMapper.entityToResponse(user);
    }
}
