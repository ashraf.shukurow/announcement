package com.ingressapp.announcement.service.impl;

import com.ingressapp.announcement.mapper.AnnouncementMapper;
import com.ingressapp.announcement.repository.UserRepository;
import com.ingressapp.announcement.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final AnnouncementMapper announcementMapper;
}
