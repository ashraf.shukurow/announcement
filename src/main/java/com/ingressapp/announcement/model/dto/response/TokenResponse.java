package com.ingressapp.announcement.model.dto.response;

import lombok.Data;

@Data
public class TokenResponse {
    private String accessToken;
}
