package com.ingressapp.announcement.model.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResponse  implements Serializable {
    private Long id;
    private String name;
    private String username;
    private String surname;
    private int age;
}
