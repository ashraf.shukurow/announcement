package com.ingressapp.announcement.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class NotAuthenticatedException extends RuntimeException {
    private String message;

}
