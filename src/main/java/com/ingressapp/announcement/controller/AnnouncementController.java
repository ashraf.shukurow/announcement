package com.ingressapp.announcement.controller;

import com.ingressapp.announcement.mapper.UserMapper;
import com.ingressapp.announcement.model.dto.request.AnnouncementRequest;
import com.ingressapp.announcement.model.dto.request.PageDto;
import com.ingressapp.announcement.model.dto.request.SearchCriteria;
import com.ingressapp.announcement.model.dto.response.AnnouncementResponse;
import com.ingressapp.announcement.repository.UserRepository;
import com.ingressapp.announcement.service.AnnouncementService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/announcement")
public class AnnouncementController {
    private final AnnouncementService announcementService;
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void createAnnouncement(@RequestBody AnnouncementRequest announcementRequest){
        announcementService.createAnnouncement(announcementRequest);
    }
    @GetMapping("/")
    public ResponseEntity<Page<AnnouncementResponse>> getAllAnnouncements(@RequestBody List<SearchCriteria> searchCriteriaList, Pageable pageable){
        return ResponseEntity.ok(announcementService.getAllAnnouncement(searchCriteriaList,pageable));
    }
    @GetMapping("/mostViewed/")
    public ResponseEntity<Page<AnnouncementResponse>> getMostViewedAnnouncements(@RequestBody PageDto pageDto){
        return ResponseEntity.ok(announcementService.getMostViewedAnnouncements(pageDto));
    }
    @GetMapping("/own/{userId}")
    public ResponseEntity<Page<AnnouncementResponse>> getAllOwnAnnouncement(@PathVariable Long userId,Pageable pageable){
        return ResponseEntity.ok(announcementService.getAllOwnAnnouncement(userId,pageable));
    }
    @GetMapping("/own/{announcementId}/{userId}")
    public ResponseEntity<AnnouncementResponse> getOwnAnnouncementWithId(@PathVariable Long announcementId,@PathVariable Long userId){
        return ResponseEntity.ok(announcementService.getOwnAnnouncementWithId(announcementId,userId));
    }
    @GetMapping("/own/most/{userId}")
    public ResponseEntity<AnnouncementResponse> getOwnMostViewedAnnouncement(@PathVariable Long userId){
        return ResponseEntity.ok(announcementService.getOwnMostViewedAnnouncement(userId));

    }
    @PutMapping("/{announcementId}")
    public void updateAnnouncement(@PathVariable Long announcementId,@RequestBody AnnouncementRequest announcementRequest){
        announcementService.updateAnnouncement(announcementId,announcementRequest);
    }
    @DeleteMapping("/{announcementId}")
    public void deleteAnnouncement(@PathVariable Long announcementId){
        announcementService.deleteAnnouncement(announcementId);
    }


}
