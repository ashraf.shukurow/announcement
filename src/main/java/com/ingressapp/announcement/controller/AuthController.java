package com.ingressapp.announcement.controller;

import com.ingressapp.announcement.model.dto.request.LoginRequest;
import com.ingressapp.announcement.model.dto.request.UserRequest;
import com.ingressapp.announcement.model.dto.response.TokenResponse;
import com.ingressapp.announcement.model.dto.response.UserResponse;
import com.ingressapp.announcement.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/auth")
@RequiredArgsConstructor
public class AuthController {
    private final AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<UserResponse> register(@RequestBody UserRequest userRequest){
        return ResponseEntity.ok(authService.register(userRequest));
    }
    @PostMapping("/login")
    public ResponseEntity<TokenResponse> login(@RequestBody LoginRequest loginRequest){
        return ResponseEntity.ok(authService.login(loginRequest));
    }
}
