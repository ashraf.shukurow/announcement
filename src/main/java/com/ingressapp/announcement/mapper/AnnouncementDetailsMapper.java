package com.ingressapp.announcement.mapper;

import com.ingressapp.announcement.model.dto.request.AnnouncementRequest;
import com.ingressapp.announcement.model.dto.response.AnnouncementDetailsResponse;
import com.ingressapp.announcement.model.entity.Announcement;
import com.ingressapp.announcement.model.entity.AnnouncementDetails;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AnnouncementDetailsMapper {
    AnnouncementDetails requestToEntity(AnnouncementRequest announcementRequest);
    AnnouncementDetailsResponse entityToResponse(Announcement announcement);

}
