package com.ingressapp.announcement.mapper;

import com.ingressapp.announcement.model.dto.request.AnnouncementRequest;
import com.ingressapp.announcement.model.dto.response.AnnouncementResponse;
import com.ingressapp.announcement.model.entity.Announcement;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AnnouncementMapper {
    Announcement requestToEntity(AnnouncementRequest announcementRequest);
    @Mapping(source ="user",target = "userResponse")
    AnnouncementResponse entityToResponse(Announcement announcement);
}
