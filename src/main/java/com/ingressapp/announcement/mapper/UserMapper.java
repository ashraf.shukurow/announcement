package com.ingressapp.announcement.mapper;

import com.ingressapp.announcement.model.dto.request.UserRequest;
import com.ingressapp.announcement.model.dto.response.UserResponse;
import com.ingressapp.announcement.model.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User requestToEntity(UserRequest userRequest);
    UserResponse entityToResponse(User user);
}
