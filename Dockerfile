FROM openjdk:17
COPY build/libs/announcement-0.0.1-SNAPSHOT.jar /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/announcement-0.0.1-SNAPSHOT.jar"]